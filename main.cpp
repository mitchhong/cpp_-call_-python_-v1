#include <Python.h>

void testBasicType();

int main()
{
    /// 设置Python的Home路径
    Py_SetPythonHome(L"./");
    /// Python解释器初始化
    Py_Initialize();

    /// 测试 在 C++ 中创建 和 转化 Python 中的基本类型
    testBasicType();


    /// 最后清理Python
    Py_Finalize();
    return 0;
}