#include <Python.h>
#include <iostream>
#include <string>
#include <iomanip>

void testBasicType()
{
    /// 1. 在 C++ 中创建 Python的整形
    PyObject* a_PyLong = PyLong_FromLong(1314);
    /// Python 整型检测
    bool a_isPyLong = PyLong_Check(a_PyLong);  // NOLINT
    /// 把 Python 的整形 转化到 C++
    long a_int = PyLong_AsLong(a_PyLong);
    /// 打印结果
    std::cout << "***************** Long *****************" << std::endl;
    std::cout << std::setw(8) << std::left << "a_int: " << std::setw(8) << std::left << a_int << std::setw(32)
              << std::setw(8) << std::left << "\tis long: " << std::boolalpha << std::setw(8) << std::left << a_isPyLong
              << std::endl;
    /// 及时销毁变量
    Py_XDECREF(a_PyLong);

    /// 2. 在 C++ 中创建 Python 的浮点型
    PyObject* b_PyFloat = PyFloat_FromDouble(13.14);
    /// Python 浮点型检测
    bool b_isPyFloat = PyFloat_Check(b_PyFloat);  // NOLINT
    /// 把 Python 的浮点型 转化到 C++
    double b_double = PyFloat_AsDouble(b_PyFloat);
    /// 打印结果
    std::cout << "***************** Float *****************" << std::endl;
    std::cout << std::setw(8) << std::left << "b_double: " << std::setw(8) << std::left << b_double << std::setw(32)
              << std::setw(8) << std::left << "\tis float: " << std::boolalpha << std::setw(8) << std::left << b_isPyFloat
              << std::endl;
    /// 及时销毁变量
    Py_XDECREF(b_PyFloat);

    /// 3. 在 C++ 中创建 Python 的字符串（避免用中文）
    PyObject* c_PyUnicode = PyUnicode_FromString("Mitch");
    /// Python 字符串型检测
    bool c_isPyUnicode = PyUnicode_Check(c_PyUnicode);  // NOLINT
    /// 把 Python 的字符串 转化到 C++(宽字符 16 位，wchar_t 0~65535，共65536个字符)
    wchar_t str_buf[1024]{'\0'};
    PyUnicode_AsWideChar(c_PyUnicode, str_buf, sizeof(str_buf));
    /// 打印结果
    std::cout << "***************** String(Unicode) *****************" << std::endl;
    std::wcout << std::setw(8) << std::left << "str_buf: " << std::setw(8) << std::left << str_buf << std::setw(32)
              << std::setw(8) << std::left << "\tis unicode: " << std::boolalpha << std::setw(8) << std::left << c_isPyUnicode
              << std::endl;
    /// 及时销毁变量
    Py_XDECREF(c_PyUnicode);

    /// 4. 在 C++ 中创建 Python 的 列表
    PyObject* d_PyList = PyList_New(0);                     //  list 不用在开始时固定其大小，可以随时扩充
    PyList_Append(d_PyList, PyLong_FromLong(1314));             //  追加一个整型数据到列表中
    PyList_Append(d_PyList, PyFloat_FromDouble(13.14));         //  追加一个浮点型数据到列表中
    PyList_Append(d_PyList, PyUnicode_FromString("Mitch"));  //  追加一个字符串型数据到列表中
    /// Python 列表类型检查
    bool d_isPyList = PyList_Check(d_PyList);  //  NOLINT
    /// 获取列表的大小
    Py_ssize_t size = PyList_Size(d_PyList);   //  Py_ssize_t 是 __int64，一个特别大的整形，一般情况当成 int 就好
    /// 获取 列表的元素（这样拿到的是 PyObject类型的，需要根据类型转化一下；）
    PyObject* d0_val = PyList_GetItem(d_PyList, 0);  //  获取 index=0上的元素；
    int d0 = PyLong_AsLong(d0_val);
    PyObject* d1_val = PyList_GetItem(d_PyList, 1);  //  获取 index=1上的元素；
    double d1 = PyFloat_AsDouble(d1_val);
    PyObject* d2_val = PyList_GetItem(d_PyList, 2);  //  获取 index=2上的元素；
    wchar_t d2[128]{'\0'};
    PyUnicode_AsWideChar(d2_val, d2, sizeof(d2));
    /// 修改 列表的元素
    PyList_SetItem(d_PyList, 0, PyLong_FromLong(1314520));
    /// 在列表中间 插入 元素
    PyList_Insert(d_PyList, 1, PyFloat_FromDouble(3.1));
    /// 删除 列表中的元素（把一个区间内的元素设置为 空，就是删除了）
    PyList_SetSlice(d_PyList, 0, 1, nullptr);  //  把 [0, 1) 范围内的元素置空，实际上就是删除了 index=1 上的元素。
    /// 遍历列表，很简单，通过 PyList_Size 获取列表的大小，然后用循环，循环内用 PyList_GetItem 获取元素、用 PyList_SetItem 修改元素
    std::cout << "***************** List(Unicode) *****************" << std::endl;
    for (int i = 0; i < PyList_Size(d_PyList); ++i)
    {
        PyObject* tmpObj = PyList_GetItem(d_PyList, i);
        if (PyLong_Check(tmpObj))  //  NOLINT
            std::cout << i << ": " << PyLong_AsLong(tmpObj) << std::endl;
        else if (PyFloat_Check(tmpObj))
            std::cout << i << ": " << PyFloat_AsDouble(tmpObj) << std::endl;
        else if (PyUnicode_Check(tmpObj))    //  NOLINT
        {
            wchar_t tmpBuf[1024]{'\0'};
            PyUnicode_AsWideChar(tmpObj, tmpBuf, sizeof(tmpBuf));
            std::wcout << i << L": " << tmpBuf << std::endl;
        }
        else
            std::cout << i << ": Unknown element!" << std::endl;
    }
    std::cout << "***************** ********** *****************" << std::endl;
    /// Note，在循环时最 判断一下当前的 PyObject是哪种类型，如果当前元素又是一个列表，你可以对其在用一个循环来打印一下，如下：
    PyObject* d_PyList2 = PyList_New(0);                       //  list 不用在开始时固定其大小，可以随时扩充
    PyList_Append(d_PyList2, PyLong_FromLong(1314));                //  追加一个整型数据到列表中
    PyList_Append(d_PyList2, PyLong_FromLong(1314520));             //  追加一个整型数据到列表中
    PyList_Append(d_PyList, d_PyList2);
    /// 遍历列表
    for (int i = 0; i < PyList_Size(d_PyList); ++i)
    {
        PyObject* tmpObj = PyList_GetItem(d_PyList, i);
        if (PyLong_Check(tmpObj))  //  NOLINT
            std::cout << i << ": " << PyLong_AsLong(tmpObj) << std::endl;
        else if (PyFloat_Check(tmpObj))
            std::cout << i << ": " << PyFloat_AsDouble(tmpObj) << std::endl;
        else if (PyUnicode_Check(tmpObj))    //  NOLINT
        {
            wchar_t tmpBuf[1024]{'\0'};
            PyUnicode_AsWideChar(tmpObj, tmpBuf, sizeof(tmpBuf));
            std::wcout << i << ": " << tmpBuf << std::endl;
        }
        else if (PyList_Check(tmpObj))  //  NOLINT
        {
            std::cout << i << ": ";
            /// 遍历子列表
            for (int j = 0; j < PyList_Size(tmpObj); ++j)
            {
                PyObject* tmpObjjj = PyList_GetItem(tmpObj, j);
                if (PyLong_Check(tmpObjjj))  //  NOLINT
                    std::cout << j << ": " << PyLong_AsLong(tmpObjjj) << "\t";
                else if (PyFloat_Check(tmpObjjj))
                    std::cout << j << ": " << PyFloat_AsDouble(tmpObjjj) << "\t";
                else if (PyUnicode_Check(tmpObjjj))    //  NOLINT
                {
                    wchar_t tmpBuf[1024]{'\0'};
                    PyUnicode_AsWideChar(tmpObjjj, tmpBuf, sizeof(tmpBuf));
                    std::wcout << j << ": " << tmpBuf << "\t";
                }
                /// 及时清理
                Py_XDECREF(tmpObjjj);
            }
            std::cout << std::endl;
        }
        else
            std::cout << "Unknown element!" << std::endl;
        /// 清理
        Py_XDECREF(tmpObj);
    }
    /// 及时销毁变量
    Py_XDECREF(d_PyList);

    /// 5. 在 C++ 中创建 Python 的元组，元素不可修改，所以在创建时就必须给定其大小！
    PyObject* e_tuple = PyTuple_New(3);
    /// 元组这就比 列表那个简单多了，用：
    /**
     * PyTuple_SetItem()、设置元素，用法同列表
     * PyTuple_GetItem()、设获取元素，用法同列表
     * PyTuple_Size()、   获取元组的大小，用法同列表
     * PyTuple_Check()    元组类型检查，用法同列表
     * */
    /// 及时清理
    Py_XDECREF(e_tuple);

    /// 6. 在 C++ 中创建 Python 的字典，key 和 value 都是 PyObject。
    PyObject* f_PyDict = PyDict_New();  //  后面通过 PyDict_SetItem 直接给字典新增键值对，也可以通过该函数修改键值对
    //  设置字典的 Age 为 22
    PyDict_SetItem(f_PyDict, PyUnicode_FromString("Age"), PyLong_FromLong(22));
    //  设置字典的 Name 为 Mitch
    PyDict_SetItem(f_PyDict, PyUnicode_FromString("Name"), PyUnicode_FromString("Mitch"));
    //  设置字典的 Gender 为 male
    PyDict_SetItem(f_PyDict, PyUnicode_FromString("Gender"), PyUnicode_FromString("male"));
    /// Python 字典类型检查
    bool d_isPyDict = PyDict_Check(f_PyDict);  // NOLINT
    /// 判断 字典 是否包含某个 key
    bool d_isContainWeight = PyDict_Contains(f_PyDict, PyUnicode_FromString("Weight"));
    /// 获取 字典 关键字 的 值，并转化到 C++ 中相应的类型
    PyObject* val_age = PyDict_GetItem(f_PyDict, PyUnicode_FromString("Age"));
    PyObject* val_name = PyDict_GetItem(f_PyDict, PyUnicode_FromString("Name"));
    PyObject* val_gender = PyDict_GetItem(f_PyDict, PyUnicode_FromString("Gender"));
    int age_ = PyLong_AsLong(val_age);
    wchar_t name[128]{'\0'};   PyUnicode_AsWideChar(val_name, name, sizeof(name));
    wchar_t gender[128]{'\0'}; PyUnicode_AsWideChar(val_gender, gender, sizeof(gender));
    /// 打印结果
    std::cout << "***************** Dict *****************" << std::endl;
    std::wcout << "Age: " << age_ << "\tName: " << name << "\tGender: " << gender << std::endl
               << "is dict: " << std::boolalpha << d_isPyDict << std::endl
               << "is contains Weight: " << std::boolalpha << d_isContainWeight
               << std::endl;
    /// 删除 字典中的 键值对（最好与 PyDict_Contains 一块使用，只有存在，然后再判断）
    if (PyDict_Contains(f_PyDict, PyUnicode_FromString("Gender")))
        PyDict_DelItem(f_PyDict, PyUnicode_FromString("Gender"));

    /// 遍历 key 和 value 返回所有的关键字、值，的两种方法
    //  法一：PyDict_Keys 返回 所有 key 的 PyList；PyDict_Values 返回 所有 value 的 PyList。
    {
        PyObject *keys = PyDict_Keys(f_PyDict);     /// PyList
        PyObject *values = PyDict_Values(f_PyDict); /// PyList

        wchar_t buff[1024] = {'\0'};
        std::cout << "All keys: ";
        for (Py_ssize_t i = 0; i < PyList_Size(keys); ++i) {
            PyObject *val_ = PyList_GetItem(keys, i);
            if (PyUnicode_Check(val_))  // NOLINT
            {
                PyUnicode_AsWideChar(val_, buff, sizeof(buff));
                std::wcout << buff << "\t";
            }
            else if (PyLong_Check(val_))  // NOLINT
                std::wcout <<PyLong_AsLong(val_) << "\t";
            else
                std::cout << "Unknown" << "\t";
        }

        std::cout <<"\nAll values: ";
        for (Py_ssize_t i = 0; i < PyList_Size(values); ++i) {
            PyObject *val_ = PyList_GetItem(values, i);
            if (PyUnicode_Check(val_))  // NOLINT
            {
                PyUnicode_AsWideChar(val_, buff, sizeof(buff));
                std::wcout << buff << "\t";
            }
            else if (PyLong_Check(val_))  // NOLINT
                std::wcout <<PyLong_AsLong(val_) << "\t";
            else
                std::cout << "Unknown" << "\t";
        }

        /// 清理
        Py_XDECREF(keys);
        Py_XDECREF(values);
    }
    //  法二：用 PyDict_Next 配合 while 循环来做
    {
        wchar_t buff[1024] = {'\0'};
        Py_ssize_t pos = 0;
        PyObject* key_ = nullptr;
        PyObject* value_ = nullptr;
        std::cout <<"\n(key: value): ";
        while (PyDict_Next(f_PyDict, &pos, &key_, &value_))
        {
            /// 先打印关键字
            if (PyUnicode_Check(key_))  // NOLINT
            {
                PyUnicode_AsWideChar(key_, buff, sizeof(buff));
                std::wcout << "(" << buff << ", ";
            }
            else if (PyLong_Check(key_))  // NOLINT
                std::cout << "(" << PyLong_AsLong(key_) << ", ";
            else
                std::cout << "(" << "Unknown" << ", ";

            /// 再打印值
            if (PyUnicode_Check(value_))  // NOLINT
            {
                PyUnicode_AsWideChar(value_, buff, sizeof(buff));
                std::wcout << buff << ")";
            }
            else if (PyLong_Check(value_))  // NOLINT
                std::wcout << PyLong_AsLong(value_) << ")";
            else
                std::wcout << "Unknown" << ")";
        }
        /// 清理
        Py_XDECREF(key_);
        Py_XDECREF(value_);
    }


    /// 清理（会自己判断传进来的 PyObject指针是否是空，空就不做操作）
    Py_XDECREF(f_PyDict);

}

