cmake_minimum_required(VERSION 3.15)
project(CPP_CALL_PYTHON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib)


###################################### Qt 相关 ######################################
set(Qt5_DIR "C:\\Qt\\Qt5_12_MSVC\\lib\\cmake\\Qt5") # Windows中
find_package(Qt5 COMPONENTS Core Gui Widgets REQUIRED)
set(CMAKE_AUTOMOC ON)

# 添加资源文件(添加好的资源要参与编译，所以add_library和add_executable中都可能需要加)
set(CMAKE_AUTORCC ON)

###################################### Python 库 ######################################
# 把 Python 的头文件的目录包括进来（固定的）
include_directories("D:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/Include")

# 把Python lib 目录路径 链接进来
link_directories("D:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python")
## 链接 Python lib
#link_libraries(
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/_bz2.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/_contextvars.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/_socket.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/_tkinter.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/python37.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/select.lib"
#        "PyPlayerD:/PersonalAllAboutStudy/Programming/CLion_Python_Demo/PyPlaer/Python/unicodedata.lib"
#)

# ***********************************以上都是常规的设置*********************************** #

# *************************下面是添加子目录、生成可执行程序、链接库************************** #

add_executable(CPP_CALL_PYTHON main.cpp allTest.cpp)
